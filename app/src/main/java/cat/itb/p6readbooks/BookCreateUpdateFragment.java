package cat.itb.p6readbooks;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

public class BookCreateUpdateFragment extends Fragment {
    private TextView titleCreateTextView;
    private TextView titleTextView;
    private EditText titleEditText;
    private TextView authorTextView;
    private EditText authorEditText;
    private TextView stateTextView;//para leer, leiendo, leido
    private Spinner stateBookSpinner;
    private TextView scoreTextView;
    private Spinner scoreBookSpinner;
    private Button addBtn;

    private Book book;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.create_update_book_fragment, container, false);
        titleCreateTextView = v.findViewById(R.id.add_book_title_text_view);
        titleTextView = v.findViewById(R.id.add_title_text_view);
        titleEditText = v.findViewById(R.id.add_title_edit_text);
        authorTextView = v.findViewById(R.id.add_author_text_view);
        authorEditText = v.findViewById(R.id.add_author_edit_text);
        stateTextView = v.findViewById(R.id.state_text_view_value);
        stateBookSpinner = v.findViewById(R.id.add_state_spinner);
        scoreTextView = v.findViewById(R.id.add_score_text_view);
        scoreBookSpinner = v.findViewById(R.id.add_score_spinner);
        addBtn = v.findViewById(R.id.add_button);

        final ArrayAdapter<CharSequence> stateSpinnerAdapter = ArrayAdapter.createFromResource(getContext(), R.array.status_array, android.R.layout.simple_spinner_item);
        stateSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateBookSpinner.setAdapter(stateSpinnerAdapter);
        final ArrayAdapter<CharSequence> scoreSpinnerAdapter = ArrayAdapter.createFromResource(getContext(), R.array.score_array, android.R.layout.simple_spinner_item);
        scoreSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        scoreBookSpinner.setAdapter(scoreSpinnerAdapter);
        scoreBookSpinner.setEnabled(false);
        if (getArguments() != null) {
            book = getArguments().getParcelable("book");
        }

        if (book != null) {
            addBtn.setVisibility(View.INVISIBLE);
            titleCreateTextView.setText("UPDATE BOOK");
            titleEditText.setText(book.getTitle());
            authorEditText.setText(book.getAuthor());

            if (book.getState().equalsIgnoreCase("Want to read")) {
                stateBookSpinner.setSelection(0);
            } else if (book.getState().equalsIgnoreCase("Reading")) {
                stateBookSpinner.setSelection(1);
            } else if (book.getState().equalsIgnoreCase("Read")) {
                stateBookSpinner.setSelection(2);
                scoreBookSpinner.setSelection(book.getScore());
                scoreBookSpinner.setEnabled(true);
            }
        } else {
            book = new Book();
        }

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.add_button:
                        if (titleEditText.getText().toString().isEmpty() || authorEditText.getText().toString().isEmpty()){
                            Toast.makeText(getContext(), "Require title, author and status", Toast.LENGTH_SHORT).show();
                        }else{
                            book = new Book(titleEditText.getText().toString(), authorEditText.getText().toString(), stateBookSpinner.getSelectedItem().toString(), Integer.parseInt(scoreBookSpinner.getSelectedItem().toString()));
                            BookViewModel.bookList.add(book);
                            Navigation.findNavController(v).navigate(R.id.action_bookCreateUpdateFragment_to_bookListFragment2);
                        }
                }
            }
        });
        return v;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        authorEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setAuthor(authorEditText.getText().toString());
                return false;
            }
        });

        titleEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setTitle(titleEditText.getText().toString());
                return false;
            }
        });

        stateBookSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                book.setState( stateBookSpinner.getSelectedItem().toString());
                if(position == 2) {
                    scoreBookSpinner.setEnabled(true);
                }
                else {
                    scoreBookSpinner.setEnabled(false);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Toast.makeText(getContext(), "stateBookSpinner.onNothingSelected()", Toast.LENGTH_SHORT).show();
            }
        });

        scoreBookSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                book.setScore(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}
