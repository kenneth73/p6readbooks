package cat.itb.p6readbooks;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {
    private String title;
    private String author;
    private String state;//para leer, leiendo, leido
    private int score;//0-5

    public Book(String title, String author, String state, int score) {
        this.title = title;
        this.author = author;
        this.state = state;
        this.score = score;
    }

    public Book(String title, String author, String state) {
        this.title = title;
        this.author = author;
        this.state = state;
    }

    public Book(){ }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    // P A R C E L //
    protected Book(Parcel in) {
        title = in.readString();
        author = in.readString();
        state = in.readString();
        score = in.readInt();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(state);
        dest.writeFloat(score);
    }
}
