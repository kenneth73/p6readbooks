package cat.itb.p6readbooks;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder>{
    List<Book> bookList;

    public BookAdapter(List<Book> bookList) {
        this.bookList = bookList;
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextViewValue;
        private TextView authorTextViewValue;
        private TextView stateTextViewValue;
        private TextView scoreTextViewValue;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextViewValue = itemView.findViewById(R.id.title_text_view_value);
            authorTextViewValue = itemView.findViewById(R.id.author_text_view_value);
            stateTextViewValue = itemView.findViewById(R.id.state_text_view_value);
            scoreTextViewValue = itemView.findViewById(R.id.score_text_view_value);

            itemView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onClick(View v) {
                    NavDirections navDir = BookListFragmentDirections.actionBookListFragmentToBookCreateUpdateFragment2(bookList.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(navDir);
                }
            });
        }

        public void bind(Book b) {
            titleTextViewValue.setText(b.getTitle());
            authorTextViewValue.setText(b.getAuthor());
            stateTextViewValue.setText(b.getState());
            if (b.getState().equalsIgnoreCase("read")) scoreTextViewValue.setText(b.getScore()+ "/5");
            else  scoreTextViewValue.setText("For scoring once read");

        }
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_item_fragment, parent, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book b = bookList.get(position);
        holder.bind(b);
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

}
