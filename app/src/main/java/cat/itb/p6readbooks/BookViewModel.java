package cat.itb.p6readbooks;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends ViewModel {

    static List<Book> bookList = new ArrayList<>();

    public BookViewModel() {
        String[] states = {
                "Reading",
                "Want to read",
                "Read"
        };
        String[] titles = {
                "Luces de Bohemia",
                "Crimen y Castigo",
                "100 años de soledad",
                "La casa de los espiritus",
                "Las almas muertas",
                "El Buscón",
                "La colmena",
                "Alejandro Dumas",
                "La odisea",
                "Harry Potter",
                "El Señor de los Anillos",
                "Lo que el viento se llevó",
                "El diario de Ana Frank",
                "La Biblia",
                "Hamlet"
        };
        String[] authors = {
                "Ramón del Valle Inclán",
                "Fedor Dostoeievsky",
                "Gabriel García Marquez",
                "Isabel Allende",
                "Nicolai Gogol",
                "Francisco de Quevedo",
                "Camilo José Cela",
                "El conde de Montecristo",
                "Homero",
                " J. K. Rowling",
                " J. R. R. Tolkien",
                "Margaret Mitchell",
                "Annelies Marie Frank",
                "Dios",
                "William Shakespeare"
        };
        for (int i = 0; i < authors.length; i++) {
            int state = (int) (Math.random() * (2+1));
            int score;
            if (state != 2) { //read = 2
                score = 0;
            } else {
                score = (int) (Math.random() * (5+1));
            }
            Book b = new Book(titles[i], authors[i], states[state], score);
            bookList.add(b);
        }
    }

    public List<Book> getListBooks() {
        return bookList;
    }
}
